
\section{Controllo in forza}
Nelle sezioni precedenti si è descritto la parte di controllo relativa al moto libero del UVMS. Dal momento in cui l'end-effector viene a contatto con la superficie, il sensore di forza misurerà la risultante di forza agente fino al polso (tipicamente il posto in cui è montato il sensore). Supponendo il contatto di tipo liscio, la forza misurata avrà solo componenti perpendicolari alla superficie nel punto di contatto e perciò assumerà la forma seguente:
\begin{equation}
	\bm{F}_{ext} = \bm{n}F_{ext}
\end{equation}
dove $\bm{n} = \bm{n}(\bm{x})$ è la normale nel punto di contatto $\bm{x}$ (con componenti in terna $\langle o \rangle$) e $F_{ext}$ il suo modulo.

Conoscendo la normale della forza misurata dal sensore è possibile scomporre il riferimento di velocità del  task di posizionamento all'end-effector nel modo seguente:
\begin{equation}
	\dot{\bar{\bm{x}}}_{t} = \dot{\bar{\bm{x}}}_{t}^{\parallel} + \dot{\bar{\bm{x}}}_{t}^{\perp}
\end{equation}
dove $\dot{\bar{\bm{x}}}_{t}^{\parallel}$ e $\dot{\bar{\bm{x}}}_{t}^{\perp}$ sono rispettivamente la velocità tangente e perpendicolare alla superficie nel punto di contatto. L'idea è quella di utilizzare $\dot{\bar{\bm{x}}}_{t}^{\parallel}$ come riferimento di velocità per il task di posizionamento dell'end-effector al posto di $\dot{\bar{\bm{x}}}_{t}$ (così da imporre solo movimenti tangenziali alla superficie) e lasciare il compito al controllo in forza di quanto muoversi perpendicolarmente alla superficie in modo da avere la forza desiderata. Il nuovo riferimento di velocità risulta quindi essere lo stesso di \eqref{riferimento_velocita}, ma depurato della parte perpendicolare:
\begin{equation}
\label{rif_vel_cont_forza}
	{\dot{\bar{\bm{x}}}}_{t} = {\dot{\bar{\bm{x}}}}_{t}^{\parallel}
\end{equation}


Per l'annullamento dell'errore di forza da utilizzare per il relativo controllo si è utilizzata la seguente legge di controllo PI (proporzionale e integrativo):
\begin{equation}
\label{calcolo_riferimento_forza}
	F^{*} = K_{p}e_{f}+K_{i}\int_{0}^{t}e_{f} \ dt
\end{equation} 
dove $e_{f}$ è l'errore tra la forza misurata e la forza desiderata e $K_{p}$, $K_{i}$ rispettivamente i guadagni del termine proporzionale e integrale dell'errore. Lo schema a blocchi è raffigurato in \figurename~\ref{fig:calcolo_riferimento_forza}
\begin{figure}[ht]
	\centering
	\includegraphics[scale=1.2, keepaspectratio]{controllo_forza}
	\caption{Schema a blocchi per la generazione dell'errore di forza. Il blocco SIM (simulazione) fornisce il valore della forza misurata.}
	\label{fig:calcolo_riferimento_forza}
\end{figure}

\subsection{Task allineamento contatto}
\label{Sec_task_allineamento}
Indipendentemente dallo schema di controllo in forza che verrà utilizzato, una volta a contatto con la superficie, è necessario mantenere il sensore rigidamente connesso all'organo terminale del manipolatore parallelo alla superficie stessa.

Per questo alla gerarchia di task presentata in \ref{gerarchia_task} \algname~\ref{alg:priority_task} è stato aggiunto un ulteriore task il cui scopo è quello di annullare il disallineamento angolare tra la normale della forza misurata e l'asse $z$ della terna $\langle t \rangle$:
\begin{equation}
\bm{\varrho} = \bm{0}
\end{equation}
dove $\bm{\varrho}$ è il vettore di disallineamento tra $\prescript{t}{}{\bm{n}}$ e $\prescript{t}{}{\bm{k}_{t}} \triangleq \begin{bmatrix}0& 0& 1\end{bmatrix}^{T}$. Come si può vedere in \figurename~\ref{fig:terne}, il vettore $\bm{k}_{t}$ è entrante al manipolatore, perciò il segno dei due vettori deve essere concorde per il calcolo del disallinneamento.

Il vettore di disallineamento tra due terne (o come in questo caso semplicemente tra due vettori) è in generale un vettore avente componenti sui tre assi principali della rispettiva terna su cui è proiettato. La scelta di utilizzare i vettori proiettati sulla terna $\langle t \rangle$ comporta che il vettore $\bm{\varrho}$, essendo ortogonale al piano formato da $\prescript{t}{}{\bm{n}}$, $\prescript{t}{}{\bm{k}_{t}}$, ha sempre la terza componente nulla. Per questo motivo utilizzare un task a tre dimensione comporta, oltre al mantenimento dell'orientazione della superficie dell'organo terminale approssimativamente parallela con quella della conduttura, anche l'impedimento a rotazioni lungo $\bm{k}_{t}$. Quest'ultima condizione risulta chiaramente un vincolo in più al quale non si vuole sottoporre il sistema in quanto ridurrebbe il nucleo di tale task, garantendo meno spazio ai task a priorità più bassa. 
Quindi la scelta di utilizzare il disallineamento in terna $\langle t \rangle$ è motivata dal fatto che in queste condizioni è possibile eliminare la terza componente e diminuire cosi la dimensioni del task in questione.

Per il calcolo del riferimento $\prescript{t}{}{\dot{\bar{\bm{x}}}_{ca}}$ al solito si può utilizzare la \eqref{velocita_riferimento_disuguaglianza} con $\bm{x} \equiv \bm{\varrho}$ e $\bm{x}_{0} = \begin{bmatrix}0& 0\end{bmatrix}^{T}$. Lo jacobiano per questo task è stato calcolato come segue:
\begin{equation}
	\prescript{t}{}{\bm{J}}_{ca} = (\prescript{t}{v}{\bm{R}}\prescript{v}{}{\bm{J}}_{ta})_{1,2}
\end{equation}
dove $\prescript{v}{}{\bm{J}}_{ta} \triangleq (\prescript{v}{}{\bm{J}}_{t})_{1,2,3}$ è lo Jacobiano della sola parte angolare del task di posizionamento dell'end-effector.

Il task in questione deve essere attivato non appena l'end-effecotor entra in contatto con la superficie, per questo si utilizza la seguente matrice di attivazione:
\begin{equation}
\label{matrice_attivazione}
	\bm{A_{ca}} = \begin{bmatrix}
		a &0\\
		0 &a
	\end{bmatrix}
\end{equation}
dove $a$ è definita come la  \eqref{funzione_attivazione_maggiore} avente $x_{m} = f_{min}$ e $\alpha = \delta f$. 

Il task viene inserito tra quello di manipolabilità e quello per l'assetto orizzontale del veicolo nella gerarchia di \algname~\ref{alg:priority_task}:
\begin{equation}
\label{task_allineamento}
(\bm{Q}_{p},\bm{\rho_{p}}) \leftarrow \mbox{task}(\bm{A_{ca}},\prescript{t}{}{\bm{J}}_{ca},\prescript{t}{}{\dot{\bar{\bm{x}}}}_{ca},\bm{Q}_{p},\bm{\rho}_{p})
\end{equation}

Si anticipa il fatto che, per via del metodo utilizzato per la simulazione del contatto, la forza generata da entrambe le leggi di controllo che verranno presentate nelle prossime sezioni sarà disturbata dal task di allineamento. Difatti, come si avrà modo di vedere nelle prossime sezioni, gli schemi di controllo proposti sono stati pensati per un  contatto di tipo puntuale e quindi il task di allineamento, ruotando l'orientazione della terna $\langle t \rangle$, andrà a modificare la forza generata. Tale disturbo si potrà osservare sotto forma di transitori nei grafici di forza che verranno mostrati nel capitolo successivo.

\subsection{Regolazione della forza a livello dinamico}
\label{Sec_controllo_forza_dinamico}
Una possibilità per effettuare la regolazione della forza 
%(come viene spiegato per esempio in \cite{siciliano2009robotics}) 
è quello di modificare la coppia di attuazione ai giunti \eqref{computed_torque_referenced} in:
%\begin{equation}
%	\label{controllo_forza_din}
%	\bar{\bm{m}} = \bm{A}(\bm{\vartheta})\bm{K_{v}}\dot{\bm{e}} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bar{\bm{\vartheta}}} + \bm{C}(\bm{\vartheta}) - \bm{J}^{T}\bm{f}^{*}, \quad \bm{f}^{*} \triangleq \begin{bmatrix} \bm{0} \\ \prescript{t}{}{\bm{n}}F^{*}\end{bmatrix}
%\end{equation} 
\begin{equation}
\label{controllo_forza_din}
\bar{\bm{m}} = \bm{A}(\bm{\vartheta})\bm{K}_{v}\dot{\bm{e}} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bar{\bm{\vartheta}}} + \bm{C}(\bm{\vartheta}) + \bm{J}^{T}\bm{n}F^{*}
\end{equation} 
che viene calcolata attraverso la seguente iterazione dell'algoritmo di Newton-Eulero:
\begin{equation}
\bar{\bm{m}} = \mbox{NewtonEulero}(\bm{K}_{v}\dot{\bm{e}},\bm{\dot{\bm{\vartheta}}},\bm{\vartheta},\prescript{v}{}{\bm{g}},\prescript{t}{}{\bm{n}}F^{*},\bm{0})
\end{equation}

Si può dimostrare che affinché la \eqref{controllo_forza_din} garantisca stabilità al sistema, è necessario che la $\dot{\bar{\bm{y}}}$ (e di conseguenza la  $\dot{\bar{\bm{\vartheta}}}$) generata dal livello cinematico sia congruente con il vincolo. Per questo deve valere:
\begin{equation}
	\label{congruenza_vincolo}
		\bm{n}^{T} \bm{J}_{tl} \dot{\bar{\bm{y}}} = \bm{0}
\end{equation}
dove $\bm{J}_{tl} \triangleq ({\bm{J}}_{t})_{4,5,6}$ è lo jacobiano della sola parte lineare del task di posizionamento dell'end-effector.

Siccome la regolazione della forza a livello dinamico richiede che valga la \eqref{congruenza_vincolo}, una prima possibilità sarebbe quella di proiettare la $\dot{\bar{\bm{y}}}$ nel spazio nullo di $\bm{n}^{T} \bm{J}_{tl}$. Tuttavia questo approccio rende la soluzione della gerarchia di task a livello cinematico sub-ottima.
 
Una alternativa che non introduce sub-ottimalità è quella di cercare direttamente la soluzione della gerarchia di task nel nullo di $\bm{n}^{T} \bm{J}_{tl}$. A tal fine è possibile aggiungere un task in cima alla gerarchia di \algname~\ref{alg:priority_task} il cui scopo è quello di impedire a tutti gli altri task di generare velocità nello spazio operativo che abbiano componenti lungo la direzione della normale  di forza.

Per le considerazioni appena fatte il riferimento di velocità e lo jacobiano di questo task risulteranno quindi essere:
\begin{equation}
\label{rif_vel_con_for_din}
\dot{\bar{x}}_{f} = 0
\end{equation}
\begin{equation}
\prescript{v}{}{\bm{J}_{f}} = \prescript{v}{}{\bm{n}^{T}}\prescript{v}{}{\bm{J}_{tl}}
\end{equation}

Come per il task di allineamento anche questo task ha bisogno di essere attivato non appena l'end-effector entra a contatto con la superficie. Per questo la matrice di attivazione $A_{f}$ risulterà essere la stessa di \eqref{matrice_attivazione} ma di dimensione uno, definita con lo stesso parametro $a$ utilizzato per il task di allineamento.

Il task viene quindi messo in cima alla gerarchia di task \algname~\ref{alg:priority_task}:
\begin{equation}
\label{task_controllo_forza_din}
(\bm{Q}_{p},\bm{\rho_{p}}) \leftarrow \mbox{task}(A_{f},\prescript{v}{}{\bm{J}}_{f},\dot{\bar{x}}_{f},\bm{Q}_{p},\bm{\rho}_{p})
\end{equation}
dove si osserva come a questo stadio $\bm{\rho_{p}}$ sia nullo mentre $\bm{Q}_{p} = (\bm{I} - (\bm{n}^{T}\bm{J}_{tl})^{\#}(\bm{n}^{T}\bm{J}_{tl}))$ che garantisce che tutti i successivi task possono solamente produrre velocità del sistema congruenti con il vincolo.

\subsection{Regolazione della forza a livello cinematico}
\label{Sec_controllo_forza_cinematico}
Una ulteriore possibilità, rispetto alla soluzione appena proposta, è quella di effettuare uno spostamento della regolazione della forza direttamente a livello cinematico. Tale opzione è interessante nel momento in cui il sistema robotico utilizzato non permetta di essere controllato in coppia ma solo in velocità (progetto DEXROV presentato in sezione \ref{Sec_dexrov}).

A tal scopo viene modificato il riferimento di velocità \eqref{rif_vel_con_for_din} in:
\begin{equation}
\label{rif_vel_con_for_cin}
\dot{\bar{x}}_{f} = -F^{*}
\end{equation}
dove si osserva il fatto che, per via della definizione di $F^{*}$ in \eqref{calcolo_riferimento_forza}, tale riferimento risulta essere differente da quello utilizzato per tutti gli altri task \eqref{velocita_riferimento_uguaglianza} in cui è presente solo il termine proporzionale dell'errore. Utilizzando questo riferimento la chiamata al task \eqref{task_controllo_forza_din} risulta essere la stessa.

Dal momento che tale controllo non consiste in un semplice spostamento a valle di un blocco, viene spiegata nel seguito la differenza che vi è tra i due schemi di controllo partendo da quello cinematico arrivando a quello dinamico.

Si consideri il problema di minimizzazione \eqref{min_task1} del suddetto task:
\begin{equation}
\label{min_control_force}
	\min_{\dot{\bar{\bm{y}}}}{\norm{F^{*} - \bm{n}^{T}\bm{J}_{tl}\dot{\bar{\bm{y}}}}}
\end{equation}

La soluzione del problema \eqref{min_control_force} in virtù della \eqref{soluzione_task1} risulta quindi valere:
\begin{equation}
	\begin{aligned}
		\dot{\bar{\bm{y}}} &= (\bm{n}^{T}\bm{J}_{tl})^{\#}F^{*}  + (\bm{I} - (\bm{n}^{T}\bm{J}_{tl})^{\#}(\bm{n}^{T}\bm{J}_{tl}))\dot{\bm{z}} \\
		&= \frac{\bm{J}_{tl}^{T}\bm{n}}{\bm{n}^{T}\bm{J}_{tl}\bm{J}_{tl}^{T}\bm{n}}F^{*}  + (\bm{I} - (\bm{n}^{T}\bm{J}_{tl})^{\#}(\bm{n}^{T}\bm{J}_{tl}))\dot{\bm{z}} \\
		&\triangleq \alpha(t)\bm{J}_{tl}^{T}\bm{n}F^{*} + (\bm{I} - (\bm{n}^{T}\bm{J}_{tl})^{\#}(\bm{n}^{T}\bm{J}_{tl}))\dot{\bm{z}}	
	\end{aligned}
\end{equation}	
dove si è definito:
\begin{equation}
\alpha(t) \triangleq \frac{1}{\bm{n}^{T}\bm{J}_{tl}\bm{J}_{tl}^{T}\bm{n}}	
\end{equation}
il quale è un guadagno scalare tempo variante. 

La soluzione di tutta la gerarchia di task sarà quindi composta da due parti: quella relativa al task di controllo in forza e quella relativa a tutti gli altri task presenti nella gerarchia, ossia 
\begin{equation}
\label{sol_cin_con_force}
	\dot{\bar{\bm{y}}} = \alpha(t)\bm{J}_{tl}^{T}\bm{n}F^{*} + \dot{\bar{\bm{y}}}'
\end{equation}
nella quale si ribadisce come, per via delle considerazioni fatte alla fine della sezione precedente, $\dot{\bar{\bm{y}}}'$ risulti essere congruente con il vincolo.

Utilizzando le variabili caratterizzanti del livello dinamico (vedere sezione \ref{Sec_modello_di_fossen_esteso}) la \eqref{sol_cin_con_force} è riscrivibile 
come: 
\begin{equation}
	\label{sol_cin_con_force_sost}
	\dot{\bar{\bm{\vartheta}}} = \alpha(t)\bm{J}^{T}\bm{n}F^{*} + \dot{\bar{\bm{\vartheta}}}'
\end{equation}
dove $\bm{J}$ risulta essere un semplice riordinamento delle colonne di $\bm{J}_{tl}$.

A questo punto sostituendo \eqref{sol_cin_con_force_sost} nella \eqref{computed_torque_referenced} si ottiene:
\begin{equation}
	\begin{aligned}
		\label{con_forza_cin_din}
		\bar{\bm{m}} &= \bm{A}(\bm{\vartheta})\bm{K}_{v}(\alpha(t)\bm{J}^{T}\bm{n}F^{*} + \dot{\bar{\bm{\vartheta}}}' - \dot{\bm{\vartheta}}) + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})(\alpha(t)\bm{J}^{T}\bm{n}F^{*} + \dot{\bar{\bm{\vartheta}}}') + \bm{C}(\bm{\vartheta}) \\
		&= \bm{A}(\bm{\vartheta})\bm{K}_{v}(\dot{\bar{\bm{\vartheta}}}' - \dot{\bm{\vartheta}}) + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}}) \dot{\bar{\bm{\vartheta}}}' + \bm{C}(\bm{\vartheta})+ [\bm{A}(\bm{\vartheta})\bm{K}_{v} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})]\alpha(t)\bm{J}^{T}\bm{n}F^{*} \\
		& = \bm{A}(\bm{\vartheta})\bm{K}_{v}\dot{\bm{e}}' + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}}) \dot{\bar{\bm{\vartheta}}}' + \bm{C}(\bm{\vartheta})+ [\bm{A}(\bm{\vartheta})\bm{K}_{v} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})]\alpha(t)\bm{J}^{T}\bm{n}F^{*}
	\end{aligned}
\end{equation}
dove è stato definito $\dot{\bm{e}}' \triangleq \dot{\bar{\bm{\vartheta}}}' - \dot{\bm{\vartheta}}$ il quale rappresenta l'errore di inseguimento della sola parte di velocità congruente con il vincolo. A questo punto si osserva come questa soluzione sia differente dalla \eqref{controllo_forza_din} per la presenza della matrice di guadagno tempo variante:
\begin{equation}
\label{matrice_differenza_cin_din}
	\tilde{\bm{K}} \triangleq [\bm{A}(\bm{\vartheta})\bm{K}_{v} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})]\alpha(t) =
	[\bm{A}(\bm{\vartheta}) + \frac{1}{K}_{v}\bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})]K_{v}\alpha(t)
\end{equation}
dove si è fatto uso della definizione $\bm{K}_{v} \triangleq K_{v}\bm{I}$. La matrice $\tilde{\bm{K}}$, non essendo diagonale, avrà l'effetto di ruotare il termine $\bm{J^{T}}\bm{n}F^{*}$ portando a soluzioni potenzialmente differenti rispetto a quelle di \eqref{controllo_forza_din}. Proprio questo fattore ha motivato la necessità di effettuare delle simulazioni per poter concludere che le due leggi di controllo sono comparabili dal punto di vista delle prestazioni.

Chiaramente per avere l'uguaglianza delle due formule sarebbe possibile calcolare l'inversa della \eqref{matrice_differenza_cin_din} e utilizzarla come guadagno per la \eqref{rif_vel_con_for_cin}. Tuttavia ciò richiederebbe la conoscenza del livello dinamico in una situazione in cui, come si è accennato all'inizio della sezione, si utilizza solo il livello cinematico per controllare il sistema. 

Per poter utilizzare guadagni  $K_{p}$ e $K_{i}$ di dimensione comparabile tra i due schemi di controllo, alla parte di regolazione della forza della \eqref{rif_vel_con_for_cin} viene aggiunto un guadagno $\frac{1}{K_{v}}$ così che la \eqref{con_forza_cin_din} differisca dalla \eqref{controllo_forza_din} per il solo termine:
\begin{equation}
	\tilde{\bm{K}}' = [\bm{A}(\bm{\vartheta}) + \frac{1}{K_{v}}\bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})]\alpha(t)
\end{equation}
\subsection{Task di eliminazione moti interni}
\label{Sec_task_eliminazione_moti_interni}
Dal momento che il movimento che si richiede all'end-effector è sufficientemente ripetitivo e che l'UVMS ha elevati gradi di libertà, sussiste la possibilità che vi siano ancora moti interni alla struttura i quali, dopo un certo numero di ripetizioni, possono portare il sistema in una condizione sfavorevole. Questo è particolarmente importante perché il contatto, attraverso il termine $\bm{J}^{T}\bm{f}_{ext}$, ha effetto su tutte le variabili di giunto andando così ad accentuare problemi legati a moti interni già presenti a causa di un non preciso inseguimento delle variabili.

Una possibilità per far questo è quella di utilizzare un task che imponga una postura predefinita ai primi quattro giunti del manipolatore, lasciando così agli ultimi tre la facoltà di muoversi liberamente.

Il riferimento di velocità avrà perciò il compito di annullare l'errore tra le postura desiderata e la posizione attuale dei primi quattro giunti del manipolatore:
\begin{equation}
	\dot{\bm{x}}_{pp} = \gamma \begin{bmatrix}y_{1}^{*} - y_{1}\\ y_{2}^{*} - y_{2}\\ y_{3}^{*} - y_{3}\\ y_{4}^{*} - y_{4}\\ 0\\ \vdots\\ 0 \end{bmatrix} \triangleq \gamma\bm{\varepsilon},\quad \bm{\varepsilon} \in \mathbb{R}^{13\times1}
\end{equation}
dove $\dot{y}_{1}^{*}$, $\dot{y}_{2}^{*}$, $\dot{y}_{3}^{*}$ e $\dot{y}_{4}^{*}$ sono appunto i valori della postura desiderata. Lo Jacobiano di questo task è banalmente:
\begin{equation}
\bm{J}_{pp} = 
\begin{bmatrix}
	\bm{I} & \bm{0} \\
	\bm{0} & \bm{0} 
\end{bmatrix} \in \mathbb{R}^{13\times13},\quad \bm{I} \in \mathbb{R}^{4\times4} 
\end{equation}
con $\bm{I}$ matrice identica. Dato che questo task deve rimanere sempre attivo la matrice di attivazione sarà una identica: $\bm{A}_{pp} = \bm{I}\in \mathbb{R}^{13\times13}$. La chiamata al task risulta quindi essere:
\begin{equation}
\label{task_postura}
(\bm{Q}_{p},\bm{\rho_{p}}) \leftarrow \mbox{task}(\bm{A}_{pp},\prescript{v}{}{\bm{J}}_{pp},\prescript{v}{}{\dot{\bar{\bm{x}}}}_{pp},\bm{Q}_{p},\bm{\rho}_{p})
\end{equation}

Un altra possibilità è quella di rendere il task appena descritto scalare e con obiettivo di disuguaglianza:
\begin{equation}
	\norm{\bm{\varepsilon}} \le \varepsilon
\end{equation}
quindi il riferimento di velocità e lo Jacobiano risultano essere:
\begin{align}
	&\dot{x}_{pp2} = \gamma\norm{\bm{\varepsilon}} \\
	&\bm{J}_{pp2} = 	\begin{bmatrix}
		\bm{\varepsilon}^{T}\bm{I} & \bm{0}
	\end{bmatrix} \in \mathbb{R}^{1\times13},\quad \bm{I} \in \mathbb{R}^{4\times4},\quad \bm{0} \in \mathbb{R}^{1\times9}
\end{align}
e la funzione di attivazione è della tipologia di \ref{funzione_attivazione_minore}.

Infine una terza possibilità è quella di effettuare una minimizzazione dei movimenti del veicolo. Questo task ha l'obiettivo di annullare la velocità del veicolo ed avrà di conseguenza il seguente riferimento di velocità e jacobiano:
\begin{align}
	&\dot{\bm{x}}_{mv} = \bm{0} \in \mathbb{R}^{6\times1}\\
	&\bm{J}_{mv} = 	\begin{bmatrix}
	\bm{0} & \bm{I}
	\end{bmatrix} \in \mathbb{R}^{6\times13},\quad \bm{0} \in \mathbb{R}^{6\times7},\quad \bm{I}\in \mathbb{R}^{6\times6}
\end{align}

Dal momento che questo task va mantenuto sempre attivo la matrice di attivazione sarà banalmente una identica.

La chiamata ai due task appena descritti risulta quindi essere:
\begin{align}
	\label{task_postura_scalare}
	&(\bm{Q}_{p},\bm{\rho_{p}}) \leftarrow \mbox{task}(A_{pp2},\bm{J}_{pp2},\dot{\bar{x}}_{pp2},\bm{Q}_{p},\bm{\rho}_{p})
	\\
	\label{task_minv}
	&(\bm{Q}_{p},\bm{\rho_{p}}) \leftarrow \mbox{task}(\bm{A}_{mv},\bm{J}_{mv},\dot{\bar{\bm{x}}}_{mv},\bm{Q}_{p},\bm{\rho}_{p})
\end{align}

Le simulazioni, presentate nell'apposita sezione \ref{Sec_risultato_simulazioni} consisteranno in una combinazioni dei task presentati in questa sezione.