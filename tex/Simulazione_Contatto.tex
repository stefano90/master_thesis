
\section{Simulazione contatto}
\label{simulazione_contatto}
In una situazione reale il feedback sulla forza è dato da un apposito sensore tipicamente montato a livello del polso del manipolatore in grado di rilevare tutte le sollecitazioni di tutto ciò che stanno "sotto" di lui nella catena cinematica. 

Nell'ambito della simulazione la parte relativa al contatto è stata gestita simulando ogni punto dell'ambiente (nel caso in esame solamente la conduttura) come un sistema massa-molla. Tale sistema ad una dimensione è descritto dalla seguente equazione:
\begin{equation}
\label{massa_molla_smorzatore}
f_{c} = -K_{m}x,\qquad K_{m},K_{s}>0
\end{equation} 
dove $K_{m}$ è il guadagno della molla, $x$ è la quantità di penetrazione dell'end-effector nella conduttura.

\subsection{Simulazione contatto puntuale}
Dato che i vettori/punti che verranno presentati in questa sezione (e nella sucessiva) sono tutti con coordinate in terna $\langle o \rangle$ per non appesantire troppo la notazione viene omesso la $o$ ad apice prima del vettore/punto, perciò per il generico vettore geometrico $\bm{v}$ la sua rappresentazione algebrica viene indicata come:
\begin{equation}
\bm{v} \triangleq \prescript{o}{}{\bm{v}}
\end{equation}

Per il lavoro svolto in questa tesi è stato necessario simulare il contatto puntuale tra l'end-effector e la conduttura, la quale è stata modellata come un cilindro che si estende all'infinito. Per questo è stata implementata una funzione (Alg. \ref{alg:contatto_puntuale}) la quale necessita dei seguenti parametri in ingresso: il punto a contatto $\bm{x}$ (nello specifico l'origine della terna $\langle t \rangle$), un qualsiasi punto appartenente all'asse di propagazione della conduttura $\bm{p}_{c}$, l'asse di propagazione della conduttura $\bm{a}_{c}$, il raggio della conduttura $r_{c}$  e il guadagno della molla per la modellazione del contatto $K_{m}$. 

Inizialmente a riga \ref{distance_vector} viene calcolato il vettore distanza $\bm{d}$ tra il punto a contatto $\bm{x}$ e il centro della pipe $\bm{p}_{c}$ , il quale viene poi depurato della componente parallela alla conduttura a riga \ref{distance_vector_perp}. Il vettore risultante $\bm{d}_{\perp}$ rappresenta il vettore distanza a norma minima tra la conduttura (supposta estendersi fino all'infinito) e il punto a contatto $\bm{x}$. Se la norma del vettore appena calcolato risulta minore del raggio $r_{c}$ significa che il punto a contatto sta all'interno della conduttura, perciò è neccessario calcolare il contributo di forza dovuto alla penetrazione, altrimenti la forza risultante risulterà nulla. A riga \ref{vers_distance_vector_perp} viene calcolato il versore di $\bm{d}_{\perp}$ il quale viene poi moltiplicato per $(r_{c}-\norm{\bm{d}})$ ottenendo così il vettore penetrazione. Infine a riga \ref{contact_force} viene moltiplicato il vettore penetrazione per $K_{m}$ ottenendo il contributo di forza $\bm{f}$ dovuto alla penetrazione (utilizzando la \eqref{massa_molla_smorzatore}).
\begin{algorithm} {ContattoPuntuale}[$\bm{x}$,$\bm{p}_{c}$,$\bm{a}_{c}$,$r_{c}$,$K_{m}$]
	\caption{Contatto puntuale tra end-effector e conduttura}
	\label{alg:contatto_puntuale}
	\begin{algorithmic} [1]
		\STATE{$ \bm{d} \leftarrow \bm{p}_{c}-\bm{x}$}
		\label{distance_vector}
		\STATE{$ \bm{d}_{\perp} \leftarrow \bm{d} - (\bm{d} \cdot \bm{a}_{c}) \bm{a}_{c}$}
		\label{distance_vector_perp}
		\IF{$\norm{\bm{d}} \leq r_{c}$}
		\STATE{$ \hat{\bm{d}_{\perp}} \leftarrow \frac{\bm{d}_{\perp}}{\norm{\bm{d}_{\perp}}}$}
		\label{vers_distance_vector_perp}
		\STATE{$\bm{f} \leftarrow K_{m}(r - \norm{\bm{d}_{\perp}})\hat{\bm{d}_{\perp}}$}
		\label{contact_force}
		\ELSE
		\STATE{$\bm{f} \leftarrow \bm{0}$}		
		\ENDIF
		\RETURN{$\bm{f}$}
	\end{algorithmic}
\end{algorithm}
\subsection{Simulazione contatto multipunto}
Come descritto brevemente in \ref{Sec_caso_di_studio} il caso di studio prevede il contatto (non modellabile come semplice contatto puntuale) tra il sensore e la superficie della conduttura. In questa sezione viene quindi presentato l'approccio utilizzato per la simulazione del contatto multipunto dove la superficie a contatto è supposta essere piana e circolare di 4,5 cm di raggio. 

L'idea è quella di campionare la superficie e per ogni campione calcolare il contributo di forza $\bm{F}$ utilizzando l'algoritmo presentato nella sezione precedente. Per ognuno di questi contributi è necessario spostarne il punto di applicazione in $\bm{x}$. Per il calcolo della forza risultante $\bm{F}_{ext}$ applicata in $\bm{x}$ è necessario semplicemente sommare tutti i contributi in quanto la forza applicata in un generico campione $\bm{x}' \neq \bm{x}$ risulta la stessa applicata in $\bm{x}$. Tale spostamento tuttavia induce un contributo di momento che non va trascurato ed è dato da:
\begin{equation}
\label{contributo_momento}
\bm{N} = (\bm{x}'-\bm{x}) \times \bm{F}
\end{equation}
il quale sommato a tutti gli altri contributi produrrà una risultante di momento totale $\bm{N}_{ext}$ applicata in $\bm{x}$.  La funzione implementata (Alg.~\ref{alg:contatto_multipunto}) ha gli stessi parametri  della funzione di contatto puntuale presentata nella sezione precedente con l'aggiunta dell'orientazione della terna $\langle t \rangle$ rispe,tto alla terna $\langle o \rangle$ e dei parametri $N_{c}$ e $N_{r}$ che rappresentano i numeri di campioni per cerchio e il numeri di cerchi a differente raggio. 

Inizilmente a riga \ref{init_force}, \ref{init_moment} vengono inizializzati forza $\bm{F}_{ext}$ e momento $\bm{N}_{ext}$ i quali, a questo stadio, rappresentano forza e momento calcolati nel solo punto $\bm{x}$. A riga \ref{delta_r}, \ref{current_r} vengono predisposte le variabili necessarie all'esecuzione del ciclo più esterno che ha il compito di campionare la superficie circolare in $N_{r}$ cerchi equidistanziati di $\delta_{r}$. A riga \ref{circle_func} viene utilizzata la funzione $\mbox{circle}(r,N_{c})$ che restituisce le coordinate $\bm{x}$ e $\bm{y}$ degli $N_{c}$ campioni del cerchio di raggio $r$. A tali vettori viene giustapposto un vettore di zeri a riga \ref{matrix_of_points}, così da ottenere una matrice contente le coordinate in terna $\langle t \rangle$ dei punti del campionamento appena fatto. Da riga \ref{init_internal_loop} a \ref{end_external_loop} è presente il ciclo che scorre ogni punto di tale matrice e in riga \ref{point_i} viene assegnato l'$i$-esimo a $\prescript{t}{}{\bm{p}}$ il quale viene trasformato in coordinate assolute a riga \ref{point_transf}. Viene quindi richiamata la funzione $\mbox{ContattoPuntuale}(\cdots)$ presentata nella sezione precedente a riga \ref{call_funct} per calcolare il contributo di forza $\bm{F}$ del singolo punto $\bm{p}$, il quale viene sommato alla risultante parziale di forza a riga \ref{calc_force}. Infine a riga \ref{calc_moment} viene sommato il contributo di momento risultante relativo a $\bm{p}$ utilizzando la relazione \eqref{contributo_momento}.

\begin{algorithm} {ContattoMultipunto}[$\bm{x}$,$\prescript{o}{t}{\bm{R}}$,$\bm{p}_{c}$,$\bm{a}_{c}$,$r_{c}$,$K_{m}$,$N_{c}$,$N_{r}$]
	\caption{Contatto multipunto tra una superficie circolare e la conduttura}
	\label{alg:contatto_multipunto}
	\begin{algorithmic} [1]
		\STATE{$\bm{F}_{ext} \leftarrow \mbox{ContattoPuntuale}(\bm{x},\bm{p}_{c},\bm{a}_{c},r_{c},K_{m})$}
		\label{init_force}
		\STATE{$\bm{N}_{ext} \leftarrow \bm{0}$}
		\label{init_moment}
		\STATE{$\delta_{r} \leftarrow \frac{4,5}{N_{r}}$}
		\label{delta_r}
		\STATE{$r \leftarrow \delta_{r}$}
		\label{current_r}
		\FOR{$r < 4,5$}
		\STATE{$(\bm{x},\bm{y}) \leftarrow \mbox{circle}(r,N_{c})$}
		\label{circle_func}
		\STATE{$\bm{P} \leftarrow 	\begin{bmatrix}
			\bm{x} &\bm{y} &\bm{0}
			\end{bmatrix}^{T}$}
		\label{matrix_of_points}
		\STATE{$i \leftarrow 1$}
		\FOR{$i < N_{c}$}
		\label{init_internal_loop}
		\STATE{$\prescript{t}{}{\bm{p}} \leftarrow \bm{P}^{i}$}
		\label{point_i}
		\STATE{$\bm{p} \leftarrow \prescript{o}{t}{\bm{R}} \prescript{t}{}{\bm{p}} + \bm{x}$}
		\label{point_transf}
		\STATE{$\bm{F} \leftarrow \mbox{ContattoPuntuale}(\bm{p},\bm{p}_{c},\bm{a}_{c},r_{c},K_{m})$}
		\label{call_funct}
		\STATE{$\bm{F}_{ext} \leftarrow \bm{F}_{ext} + \bm{F}$}
		\label{calc_force}
		\STATE{$\bm{N}_{ext} \leftarrow \bm{N}_{ext} + (\bm{p}-\bm{x}) \times \bm{F}$}
		\label{calc_moment}
		\STATE{$i \leftarrow i + 1$}
		\ENDFOR
		\label{end_external_loop}
		\STATE{$r \leftarrow r + \delta_{r}$}
		\ENDFOR
		\RETURN{$(\bm{F}_{ext},\bm{N}_{ext})$}
	\end{algorithmic}
\end{algorithm}

Come già anticipato alla fine di sezione \ref{Sec_task_allineamento}, questo tipo di approccio per la gestione del contatto insieme al utilizzo del task di allineamento \eqref{task_allineamento} produrrà dei transitori visibili nei grafici di forza presenti in fondo a questo capitolo. Dovrebbe essere ormai chiaro come il task di allineamento, ruotando l'end-effector in modo tale da metterlo tangente alla superficie di contatto, modifichi implicitamente anche la forza risultante calcolata in $\bm{x}$.

A questo punto per la \eqref{simulazione} viene utilizzata:
\begin{equation}
	\tilde{\bm{m}} \triangleq  \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bm{\bm{\vartheta}}} + \bm{C}(\bm{\vartheta}) - \bm{J}^{T}\bm{f}_{ext},\quad \bm{f}_{ext} \triangleq \begin{bmatrix} \bm{N}_{ext} \\\bm{F}_{ext} \end{bmatrix}
\end{equation}
la quale viene generata con la seguente iterazione del Newton Eulero:
\begin{equation}
	\tilde{\bm{m}} = \mbox{NewtonEulero}(\bm{0},\dot{\bm{\vartheta}},\bm{\vartheta},\prescript{v}{}{\bm{g},-\prescript{}{}{\bm{F}_{ext}},-\prescript{}{}{\bm{N}_{ext}}})
\end{equation} 
