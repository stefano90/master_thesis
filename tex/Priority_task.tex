
\section{Struttura di controllo a Priorità di Task}
\label{Sec_priority_task}

\subsection{Introduzione}
L'idea che sta alla base del controllo a priorità di task \cite{Nakamura01061987} è quella di sfruttare la ridondanza cinematica di una sistema robotico al fine di far eseguire un task primario, come per esempio il raggiungimento dell'end-effector di un postura desiderata, soddisfacendo allo stesso tempo altri task secondari legati alla sicurezza. Esempi di questi ultimi possono essere il mantenimento della giusta distanza dei giunti dai loro fine corsa, il mantenimento dell'indice di manipolabilità sopra una certa soglia (in modo cosi da non indurre elevate velocità ai giunti dovute ad una posizione "scomoda" del manipolatore), ecc.
La procedura, una volta data ad ogni task una priorità, risolve il problema cinematico inverso cercando le soluzioni del generico task all'interno della ridondanza fornita dai task a priorità più alta. 

La versione utilizzata in questa tesi proposta in \cite{priorityTaskSimetti1} ed estesa \cite{priorityTaskSimetti2} è una variante di quella originale che grazie all'introduzione delle funzioni di attivazione, le quali permettono di attivare/disattivare un task senza creare discontinuità nelle variabili di controllo, permette di eseguire in maniera efficace i task relativi a controlli di disuguaglianza. Conseguenza diretta dell'utilizzo di questa versione è la possibilità di mettere ai primi posti della gerarchia quei task relativi alla sicurezza del manipolatore (come per esempio i limiti di giunti) senza inficiare il soddisfacimento dell'obiettivo primario della missione (per esempio il raggiungimento di una posa desiderata da parte dell'end-effector). 

La sezione è organizzata come segue: viene inizialmente proposta la soluzione del problema a due task al fine di comprendere meglio i concetti principali per poi estendere i risultati al problema con più task. Viene successivamente introdotto il concetto di funzione di attivazione ed infine viene riportato l'algoritmo proposto in \cite{priorityTaskSimetti2} ed utilizzato nell'algoritmo di controllo implementato per questa tesi.

\subsection{Soluzione del problema a due task}
Si suppone di avere 2 task $ T_{1} = (\bm {J_{1}},\dot{\bar{\bm x}}_{1}) $ e $ T_{2} = (\bm J_{2},\dot{\bar{\bm x}}_{2}) $ con $ \bm J_{1} $ e $ \bm J_{2} $ i relativi Jacobiani e $ \dot{\bar{\bm x}}_{1} $ e $ \dot{\bar{\bm x}}_{2} $ le relative velocità di riferimento. $ T_{1} $ è supposto essere a priorità più alta di $ T_{2} $. Trovare la soluzione del primo task consiste nel trovare $ \dot{\bm y} $ che meglio approssimi la velocità di riferimento nello spazio operativo $ \dot{\bar{\bm x}}_{1} $. Un possibile criterio è quello di minimizzare l'\textit{errore quadratico medio}, ossia:
\begin{equation}
\label{min_task1}
\min_{\dot{\bm y}} \norm{\dot{\bar{\bm x}}_{1} - J_{1}\dot{\bm y}}^{2}
\end{equation}

Dalla letteratura è noto che la soluzione è data da:
\begin{equation}
\label{soluzione_task1}
\dot{\bm y} = {\bm J_{1}}^{\#}\dot{\bar{\bm x}}_{1} + (\bm I -  {\bm J_{1}}^{\#} \bm J_{1})\dot{\bm z}, \qquad \forall \dot{\bm{z}}
\end{equation}
con $ \bm J^{\#} $ inversa generalizzata di $ \bm J $ e $ \dot{\bm z} $ vettore arbitrario. Il termine $ {\bm J_{1}}^{\#}\dot{\bar{\bm x}}_{1} $ rappresenta la soluzione a norma minima di \eqref{min_task1}, mentre $ (\bm I -  {\bm J_{1}}^{\#}\bm J_{1}) $ rappresenta il proiettore nel nucleo di $\bm{J}_{1}$, ossia lo spazio di soluzioni che non inducono velocità al task è che quindi non influiscono nella soluzione a norma minima. Analogamente per il task $ T_{2} $, la soluzione va trovata come:
\begin{equation}
\label{min_task2}
\min_{\dot{\bm y}} \norm{\dot{\bar{\bm x}}_{2} - \bm J_{2}\dot{\bm y}}^{2}
\end{equation}

Sfruttando l'arbitrarietà della soluzione nella \eqref{soluzione_task1} garantita da $ \dot{\bm z} $ si sostituisce la \eqref{soluzione_task1} nella \eqref{min_task2}, trovando cosi il nuovo problema che tiene conto di entrambi i task:
\begin{equation}
\label{min_task1_2}
\min_{\dot{\bm z}} \norm{\dot{\tilde{\bm x}}_{2} - \hat{\bm J_{2}}\dot{\bm z}}^{2}
\end{equation}
avendo definito opportunamente:
\begin{equation}
\label{def_xtilde_Jhat}
\dot{\tilde{\bm x}}_{2} = \dot{\bar{\bm x}}_{2} - \bm J_{2}\bm J_{1}^{\#}\dot{\bar{\bm x}}_{1}, \qquad \hat{\bm J}_{2} = \bm J_{2}(\bm I - \bm J_{1}^{\#}\bm J_{1})
\end{equation}
la soluzione al nuovo problema \eqref{min_task1_2} a questo punto vale: 
\begin{equation}
\label{soluzione_task2}
\dot{\bm z} = \hat{\bm J_{2}}^{\#}\dot{\tilde{\bm x}}_{2} + (\bm I -  \hat{\bm J_{2}}^{\#} \hat{\bm J_{2}})\dot{\bm w}, \qquad \forall \dot{\bm w}
\end{equation}
sostituendo la \eqref{soluzione_task2} nella \eqref{soluzione_task1} (trascurando il secondo termine utile solo nel caso in cui vi sia almeno un altro task a priorità pi bassa)  e ricordando le definizione fatte in \eqref{def_xtilde_Jhat} si trova infine:
\begin{equation}
\label{soluzione_task1_2}
\dot{\bm y} = {\bm J_{1}}^{\#}\dot{\bar{\bm x}}_{1} + [\bm J_{2}(\bm I-{\bm J_{1}}^{\#}\bm J_{1})]^{\#}(\dot{\bar{\bm x}}_{2} - \bm J_{2}\bm J_{1}^{\#}\dot{\bar{\bm x}}_{1})
\end{equation}
la quale fornisce la soluzione del problema complessivo.

\subsection{Soluzione del problema a più task}
La soluzione del problema a più task è stata formulata in \cite{Hanafusa1981} e viene qui riportata per completezza:
\begin{equation}
\label{soluzione_ntask}
\dot{\bm y}_{i} = \dot{\bm y}_{i-1} + (\bm J_{i}\bm P_{i-1}^{A})^{\#}(\dot{\bar{\bm x}}_{i}-\bm J_{i}\dot{\bm y}_{i-1}), \qquad \dot{\bm y}_{1} = {\bm J_{1}}^{\#}\dot{\bar{\bm x}}_{1}
\end{equation}
dove 
\begin{equation}
\bm P_{i-1}^{A} = \bm I - {\bm J_{i}^{A}}^{\#}{\bm J_{i}^{A}}, \qquad \bm J_{i}^{A} = 
\begin{bmatrix} \bm J_{1}  \\ 
\bm J_{2}  \\ 
\vdots \\
\bm J_{i}
\end{bmatrix}
\end{equation}
è il proiettore nel nucleo dello Jacobiano aumentato $ \bm J_{i}^{A} $. Si osserva come $ \bm J_{i}\bm P_{i-1}^{A} $ permetta al i-esimo task di essere eseguito solamente in quelle direzioni che "non disturbano" gli $i$-1 task a priorità più alta. Un altra considerazione è che nonostante $ \bm J_{i} $ non sia singolare, il termine $ \bm J_{i}\bm P_{i-1}^{A} $ potrebbe esserlo, portando cosi a quella che viene chiamata \textit{singolarità algoritmica}. Nelle vicinanze di quest'ultima, il comportamento è analogo a quello ottenuto in prossimità della singolarità cinematica, ossia viene indotta un elevata velocità ai giunti in corrispondenza di una bassa velocità del task. Per risolvere questo tipo di problema un possibilità è quella di effettuare una regolarizzazione modificando il problema originario \eqref{min_task1} per il generico task in:
\begin{equation}
\label{min_regolarizzata}
\min_{\dot{\bm{y}}}{\begin{bmatrix} \norm{\dot{\bar{\bm x}} - \bm J\dot{\bm y}}^{2} +\lambda^{2}\norm{\dot{\bm{y}}}^{2}\end{bmatrix}}
\end{equation}
a cui è stato aggiunto il secondo termine $ \lambda^{2}\norm{\dot{\bm{y}}}^{2} $ il cui scopo è quelli di mantenere dei valori bassi di $ \dot{\bm y} $. Un valore basso del parametro $ \bm \lambda $ tende a privilegiare il problema originario, un valore alto invece tende a mantenere $ \dot{\bm{y}} $ più limitato. La soluzione a norma minima di questo nuovo problema è:
\begin{equation}
\label{def_DLS-inverse}
\dot{\bm y} = \bm J^{\#\bm \lambda}\dot{\bar{\bm x}}, \qquad \bm J^{\#\bm \lambda} = (\bm J^{T}\bm J + \bm \lambda \bm I)^{\#}\bm J^{T}
\end{equation}
con $ \bm J^{\#\bm \lambda}\ $ che prende il nome di \textit{Damped Least-Squares Inverse} (DLS-Inverse).

Il problema della \eqref{min_regolarizzata} sta nel fatto che il termine $ \lambda^{2}\norm{\dot{\bm{y}}}^{2} $ tende a regolarizzare tutte le componenti di $ \dot{\bm{y}} $ indipendentemente da quale riga di $ \bm{J} $ si sta avvicinando alla singolarità. Una possibilità è quella di effettuare una regolarizzazione che vada ad agire solo in quelle direzioni di $ \bm{J} $ che stanno portando alla singolarità. 

Viene fatta la decomposizione di $ \bm{J} $ in valori singolari (SVD), ossia $ \bm{J} = \bm{U\Sigma V^{T}}$, con $ \bm{U} \in \mathbb{R}^{m \times m} $ e $ \bm{V} \in \mathbb{R}^{n \times n} $ matrici ortonormali e $ \bm{\Sigma} \in \mathbb{R}^{m \times n} $ matrice rettangolare diagonale, i cui elementi sono proprio i valori singolari. Viene modificato il problema \eqref{min_task1} in:
\begin{equation}
\label{min_regolarizzata_svd}
\min_{\dot{\bm{y}}}{\begin{bmatrix} \norm{\dot{\bar{\bm x}} - \bm J\dot{\bm y}}^{2} + \norm{\bm{V}^{T}\dot{\bm{q}}}^{2}_{\bm{P}} \end{bmatrix}}
\end{equation}
dove il termine $ \norm{\bm{V}^{T}\dot{\bm{q}}}^{2}_{\bm{P}} $ è la norma pesata, ossia $ \dot{\bm{q}}^{T}\bm{V}\bm{P}\bm{V}^{T}\dot{\bm{y}} $ e $ \bm{P} \in \mathbb{R}^{n \times n} $ è una matrice diagonale i cui elementi sono i fattori di regolarizzazione dipendenti dal valore singolare. 

La soluzione a norma minima del problema \eqref{min_regolarizzata_svd} è:
\begin{equation}
\dot{\bm{y}} = \bm{V}(\bm{\Sigma}^{T}\bm{\Sigma} + \bm{P})^{\#}\bm{\Sigma}^{T}\bm{U}^{T}\dot{\bar{\bm{x}}}
\end{equation}
Una caratteristica molto importante di questa soluzione è che la quantità $ (\bm{\Sigma}^{T}\bm{\Sigma} + \bm{P}) $ è sempre a rango costante $ m $ il che risolve i problemi di discontinuità a cui si è fatto riferimento fino ad ora. Questo tipo di regolarizzazione prende il nome di Singular Value Oriented Regularization (SVO).

\subsection{Estensione con funzioni di attivazione}
Come già accennato nell'introduzione del capitolo è importante considerare il caso in cui, per una qualsiasi motivazione, un task debba essere attivato/disattivato senza provocare discontinuità nelle variabili di controllo. Questa situazione si può presentare o perché l'obiettivo di controllo associato risulta già soddisfatto o perché in una particolare fase della missione il task non debba essere considerato.

L'idea è quella di definire uno scalare $ 0 < a_{(j)} < 1 $  per il generico i-esimo task scalare con il seguente significato:
\begin{itemize}
	\item $ a_{(j)} = 0 $ task inattivo;
	\item $ a_{(j)} = 1 $ task attivo;
	\item $ 0 < a_{(j)} < 1 $ task in transizione.
\end{itemize}

Si consideri un generico task multidimensionale $\bm{x}$ e l'obiettivo associato alla $j$-esima componente $x_{(j)} \leq x_{(j),M}$. La funzione di attivazione relativa a questa componente è definita come:
\begin{align}
	\label{funzione_attivazione_minore}
	a_{(j)} \triangleq
	\begin{cases} 
		1, \qquad &x_{(j)} >  x_{(j),M} \\ 
		s_{j}(x), \qquad &x_{(j),M} - \beta_{(j)} \leq  x_{(j)} \leq x_{(j),M} \\
		0, \qquad  &x_{(j)} <  x_{(j),M} - \beta_{(j)}
	\end{cases}
\end{align}
analogamente per l'obiettivo $x_{(j)} \geq x_{(j),m}$ si definisce
\begin{align}
	\label{funzione_attivazione_maggiore}
	a_{(j)} \triangleq
	\begin{cases} 
		0, \qquad &x_{(j)} >  x_{(j),m} + \beta_{(j)}\\ 
		s_{j}(x), \qquad &x_{(j),m} \leq  x_{(j)} \leq x_{(j),m} + \beta_{(j)} \\
		1, \qquad  &x_{(j)} <  x_{(j),m}
	\end{cases}
\end{align}
dove $ s_{j}(x) $ è una funzione sigmoidale e $ \beta_{(j)} $ è la zona di transizione dove l'obiettivo di disuguaglianza è soddisfatto, ma il valore di attivazione è maggiore di zero. Questo è necessario per prevenire ai fenomeni di chattering vicino a $x_{(j),M}$ (o $x_{(j),m}$), ossia uno stato di instabilità in cui il sistema passando da task attivo a task inattivo e viceversa, crea delle oscillazioni ad alta frequenza sull'uscita. 

Un esempio di funzione di attivazione per l'obiettivo $ x_{(j)} \leq x_{(j),M} $ è mostrata in \figurename~\ref{fig:funzione_di_attivazione}.
\begin{figure}[th]
	\centering
	\includegraphics[scale=1.5, keepaspectratio]{funzione_attivazione}
	\caption{Esempio di funzione di attivazione relativa all'obiettivo di controllo $ x_{(j)} \leq x_{(j),M} $ in cui $ x_{(j),M} = 6 $ e $ \beta_{(j)} = 2 $}
	\label{fig:funzione_di_attivazione}
\end{figure}

Il problema originario per l'i-esimo task \eqref{min_task1} viene trasformato in :
\begin{equation}
\label{min_taski_attivazione}
\min_{\dot{\bm y}} \norm{\bm A(\dot{\bar{\bm x}}_{i} - \bm J_{i}\dot{\bm y})}^{2}
\end{equation}
dove $ \bm{A} $ è  una matrice diagonale composta dagli $ a_{(j)} $ appena definiti. La soluzione per questo problema vale:
\begin{equation}
\dot{\bm y} = (\bm A\bm J)^{\#}\bm A\dot{\bar{\bm x}} + (\bm I - (\bm A\bm J)^{\#}\bm A\bm J)\dot{\bm z}, \qquad \forall \dot{\bm z}
\end{equation}
la quale può presentare una discontinuità nella soluzione nel caso in cui un elemento $ A_{i,i} $ passa da $ 0 $ a $ \epsilon > 0 $ o viceversa. Per questo conviene utilizzare una regolarizzazione della soluzione con la DLS o SVO, dove quest'ultima è preferita per le motivazioni presentate nella sezione precedente. Tuttavia in entrambi i casi, dato che non c'è una relazione diretta tra i valori di attivazione e quelli di regolarizzazione, è richiesto o di avere alti valori di regolarizzazione, i quali hanno un impatto negativo nelle performance, o piccoli, i quali non prevengono al problema delle discontinuità pratiche (ossia continue a livello matematico ma discontinue implementata su calcolatore). 

La soluzione a questo problema è stata proposta in \cite{priorityTaskSimetti1} ed estesa \cite{priorityTaskSimetti2} e prevede l'uso di una pseudo inversa:
\begin{equation}
\bm{X}^{\#,\bm{A},\bm{Q}}  \triangleq \left(\bm{X}^T\bm{A}\bm{X} + (\bm{I} -\bm{Q})^T(\bm{I} -\bm{Q}) + \bm{V}^T\bm{P}\bm{V}\right)^\# \bm{X}^T \bm{A} \bm{A} 
\end{equation}
dove $ \bm{Q} $ è il proiettore nel nucleo dei task a priorità più alta.

\subsection{Soluzione problema a più task con funzioni di attivazione}
Utilizzando la definizione di pseudoinversa fatta nella sezione precedente, seguono i risultati di \cite{priorityTaskSimetti2}. 

Si inizializza
\begin{equation}
\begin{aligned}
\bm{\rho}_{0} &= \bm{0}, & \bm{Q}_{0} &= \bm{I}, 
\end{aligned}
\end{equation}
per $k = 1, \ldots, p$, dove $p$ è il numero totale di livelli di priorità e $\bm{\rho}$ è la soluzione:
\begin{equation}\label{eq_tri_centralized_alg}
\begin{aligned}
\bm{W}_k &=  \bm{J}_k \bm{Q}_{k-1} (\bm{J}_k \bm{Q}_{k-1})^{\#,\bm{A}_k,\bm{Q}_{k-1}} \\
\bm{Q}_k &= \bm{Q}_{k-1} (\bm{I} - (\bm{J}_k \bm{Q}_{k-1})^{\#,\bm{A}_k,\bm{I}} {\bm{J}_k \bm{Q}_{k-1}}) \\
\bm{T}_k &\triangleq (\bm{I} - \bm{Q}_{k-1} (\bm{J}_k \bm{Q}_{k-1})^{\#,\bm{A}_k,\bm{I}} \bm{W}_k \bm{J}_k)\\
\bm{\rho}_k &= \bm{T}_k \bm{\rho}_{k-1} + \bm{Q}_{k-1} (\bm{J}_k \bm{Q}_{k-1})^{\#,\bm{A}_k,\bm{I}} \bm{W}_k \dot{\bar{\bm{x}}}_k
\end{aligned}
\end{equation}
infine la soluzione finale è data da:
\begin{equation}
\dot{\bm{y}} = \bm{\rho}_p
\end{equation}