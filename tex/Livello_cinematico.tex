\section{Livello Cinematico}
In questa sezione verranno inizialmente specificate le variabili definite in sezione \ref{Sec_definizioni} nel caso di UVMS, dopo di che verranno elencati gli obiettivi di controllo utilizzati per il priority task. Successivamente verrà descritta la tecnica di inseguimento del percorso da parte dell'end-effector ed infine verrà presentata la gerarchia di task con le rispettive variabili (rifermenti di velocità, funzioni di attivazione e Jacobiani).

\subsection{Definizioni}
Si definisce il \textit{vettore configurazione} dell'UVMS come:
\begin{equation}
\label{def_configurazione}
\bm c \triangleq
\begin{bmatrix} 
\bm q 	 \\
\bm \eta \\
\end{bmatrix}
\in \mathbb{R}^{nx1}
\end{equation}
dove $ \bm q \in \mathbb{R}^{lx1} $ è il \textit{vettore configurazione del braccio} e $ \bm \eta \in \mathbb{R}^{6x1} $ è il \textit{vettore posizione alle cooordinate generalizzate del veicolo}, composto da $ \bm{\eta}_{1} $ posizione del veicolo rispetto alla terna mondo $ \langle o \rangle  $ e $ \bm \eta_{2} $ orientazione del veicolo espressa in termine dagli angoli roll, pitch e yaw applicati in sequenza \cite{rpy_angles}.

Si definisce il \textit{vettore velocità} dell'UVMS come:
\begin{equation}
\label{def_velocita}
\dot{\bm y} \triangleq
\begin{bmatrix} 
\dot{\bm q} 	 \\
\bm{\nu} \\
\end{bmatrix}
\in \mathbb{R}^{nx1}
\end{equation}
dove $ \dot{\bm q} \in \mathbb{R}^{lx1} $ è il \textit{vettore velocità ai giunti} e $ \bm{\nu} \in \mathbb{R}^{6x1} $ è il \textit{vettore velocità del veicolo} composto da $ \bm{\nu}_{1}$ velocità lineare e $ \bm{\nu}_{2} $ velocità angolare, entrambe con componenti su $\langle v \rangle$ (terna veicolo).

\subsection{Obiettivi di controllo}
\label{Subsec_obbiettivi_di_controllo}
Si vanno ad elencare gli obiettivi comuni agli schemi di controllo proposti nelle sezioni successive:
\begin{itemize}
	\item \textit{Limiti di giunto}: il manipolatore deve operare all'interno dei suoi limiti di giunto, il che comporta il soddisfacimento di due obiettivi di disuguaglianza:
	\begin{equation}
		\begin{cases} 
			q_{i} < q_{i,M} \\ 
			q_{i} > q_{i,m} \\
		\end{cases}
		\qquad i = 1,\ldots,l
	\end{equation}
	dove $ q_{i} $ è l'i-esima coordinata di giunta, $ q_{i,M} $ il suo valore massimo e $ q_{i,m} $ il suo valore minimo.
	\item \textit{Destrezza}: il manipolatore deve mantenere una buona destrezza in modo tale da eseguire le operazioni necessarie senza incorrere in problemi legati alla singolarità. Per questo è necessario che l'indice di manipolabilità $ \mu $ \cite{Hanafusa1981} sia mantenuto sopra una certa soglia:
	\begin{equation}
		\mu > \mu_{0}
	\end{equation}	
	\item \textit{Assetto orizzontale}: l'assetto del veicolo deve essere mantenuto entro certi limiti per evitare il capovolgimento dello stesso. Tuttavia è consigliato che tali limiti siano sufficientemente grandi, in modo da evitare eccessivi consumi di energia per mantenere perfettamente orizzontale il veicolo:
	\begin{equation}
		\norm{\bm \varepsilon} < \varepsilon_{M}
	\end{equation}
	dove $ \bm \varepsilon $ è il vettore di disallinemanto tra l'asse z del del frame mondo $ \langle w \rangle $ e l'asse z del frame veicolo $ \langle v \rangle $ e $ \varphi_{M} $ è il modulo del suo valore massimo.
	\item \textit{Posizionamento end-effector}: la terna utensile $ \langle t \rangle $ rigidamente connessa alla terna dell'end-effector $ \langle e \rangle $, deve convergere verso una terna obiettivo $ \langle g \rangle $. Per questo si devono soddisfare i seguenti obiettivi di uguaglianza:
	\begin{equation}
	r_{i} = 0, \quad \theta_{i} = 0, \qquad i = 1,2,3
	\end{equation}	
	dove $ \bm r $ e $ \bm \theta $ sono rispettivamente i disallinementi di posizione ed orientazione tra le terne $ \langle t \rangle $ e $ \langle g \rangle $.
\end{itemize}

\subsection{Inseguimento percorso da parte dell'end-effector}
\label{Sec_inseguimento_percorso}
\begin{figure}[bt]
	\centering
	\includegraphics[scale=1.7, keepaspectratio]{parallel_swath}
	\caption{Esempio di percorso a \textit{parallel swath} sul piano $x$-$y$ che inizia  da $(-1.94,10.41)$ e evolve di $0.5$ m sulla $y$ e $0.2$ m sulla $x$.}
	\label{fig:parallel_swath}
\end{figure}
Avendo a disposizione solo parzialmente le informazioni sulla conduttura (si hanno solamente posizione e dimensione approssimative), non è possibile definire esplicitamente il percorso che segua il profilo della conduttura. Per questo motivo il percorso desiderato da dare in ingresso al livello cinematico  è un percorso sul piano $x $-$y$ che sta al di sotto della conduttura. Un esempio di tale percorso è mostrato in \figurename~\ref{fig:parallel_swath}. Più precisamente quello che viene dato in ingresso al livello cinematico, sono solamente i punti intermedi di tale percorso, chiamati \textit{waypoint}. Facendo riferimento all'esempio di percorso in \figurename~\ref{fig:parallel_swath}, l'algoritmo \ref{alg:inseguimento_percorso} mostra la procedura di aggiornamento dell'obietivo, dove $\bm{\varphi} \triangleq \begin{bmatrix} \prescript{o}{}{\bm{\theta}}_{t,g}^T & \prescript{o}{}{\bm{r}_{t,g}^T} \end{bmatrix}^{T}$  è il vettore di disallineamento angolare e lineare tra le terne $\langle t \rangle$ e $\langle g \rangle$, $\epsilon$ rappresenta l'errore massimo di $\norm{\bm{\varphi}}$ che si accetta per aggiornare l'obiettivo e oriz/vert sono i flag che servono a gestire rispettivamente lo spostamento dell'obiettivo sulla $x$ e sulla $y$ ($oriz \leftarrow false$ e $vert \leftarrow true$ sono le inizializzazioni per l'esempio di \figurename~\ref{fig:parallel_swath}). 
\begin{algorithm}{AggiornamentoObiettivo}[$\bm{g}$,$\bm{\varphi}$,$\mbox{oriz}$,$\mbox{vert}$]
	\caption{inseguimento percorso}
	\label{alg:inseguimento_percorso}
	\begin{algorithmic} [1]
		\IF{$\norm{\bm{\varphi}} < \epsilon$}
			\IF{$\mbox{oriz} == true$}
				\STATE{$\bm{g} \leftarrow \bm{g} - \begin{bmatrix} 0 &-0.2 &0 \end{bmatrix}^{T}$}
				\label{agg_obb1}
				\STATE{$\mbox{oriz} \leftarrow false$}
			\ELSE
				\IF{$\mbox{vertical} == true$}
					\STATE{$\bm{g} \leftarrow \bm{g} - \begin{bmatrix} 0.5 &0 &0 \end{bmatrix}^{T}$}
					\label{agg_obb2}
					\STATE{$\mbox{vert} \leftarrow -1$}
				\ELSIF{$\mbox{vertical} == -1$}
					\STATE{$\bm{g} \leftarrow \bm{g} - \begin{bmatrix} -0.5 &0 &0 \end{bmatrix}^{T}$}
					\label{agg_obb3}
					\STATE{$\mbox{vert} \leftarrow 1$}
				\ENDIF
				\STATE{$\mbox{oriz} \leftarrow true$}
			\ENDIF
		\ENDIF
		\RETURN{$(\bm{g},\mbox{oriz},\mbox{vert})$}
	\end{algorithmic}
\end{algorithm}

Volendo annullare l'errore di posizionamento $\bm{\varphi}$, il riferimento di velocità del task di posizionamento utilizzando la \eqref{velocita_riferimento_uguaglianza} risulta essere:
\begin{equation}
\label{riferimento_velocita}
	\dot{\bar{\bm{x}}}_{t} = \gamma \bm{\varphi}, \quad \gamma>0
\end{equation}

Si nota però che al momento dell'aggiornamento dell'obiettivo (righe \ref{agg_obb1},\ref{agg_obb2},\ref{agg_obb3}) $\bm{\varphi}$ (e di conseguenza $\dot{\bar{\bm{x}}}_{t}$) subisce una brusca variazione passando da un valore prossimo allo zero ad un valore proporzionale al disallineamento angolare e lineare tra $ \langle t \rangle $ e il nuovo obiettivo $ \langle g \rangle $ un instante di tempo dopo. Questa variazione per via della \eqref{soluzione_task1} si riflette sulla $ \dot{\bm{y}} $ il che richiederebbe brusche accelerazioni ai motori del braccio e alle eliche del veicolo. Questa situazioni andrebbe evitata, per questo viene proposto nel seguito la soluzione implementata.

\subsubsection{Terna virtuale}
Il problema principale della tecnica appena descritta consiste nel fatto che più il nuovo obiettivo è distante, più il riferimento di velocità subirà delle forti variazioni. Per questo l'idea è quella di diminuire queste variazione utilizzando come riferimento il disallineamento non più tra $ \langle t \rangle $ e $ \langle g \rangle $ ma tra $ \langle t \rangle  $ e una terna intermedia chiamata \textit{terna virtuale} $ \langle vrt \rangle $. Questa tecnica presentata per la prima volta da Aicardi, Casalino in \cite{aicardi_casalino} risulta adatta allo scopo in quanto è facile da implementare e soffre di alcuni svantaggi, per esempio il taglio degli angoli, che non influiscono sull'obiettivo da svolgere nel caso in esame. Inizialmente  $ \langle vrt \rangle $ viene fatta coincidere con la posizione iniziale di $ \langle t \rangle  $ dopo di che quest'ultima viene fatta muovere con una certa velocità verso $ \langle g \rangle $. Per evitare che $ \langle vrt \rangle $ si allontani troppo, quando viene superata una certa soglia, la sua velocità viene fatta diminuire. In \algname~\ref{alg:terna virtuale} viene presentato quanto appena descritto dove si è definito:
\begin{equation}
	\bm{\varphi}_{vrt,g} \triangleq \begin{bmatrix}
	\prescript{o}{}{\bm{\theta}_{vrt,g}}\\
	\prescript{o}{}{\bm{r}_{vrt,g}}
\end{bmatrix}, \quad
	\bm{\varphi} \triangleq \begin{bmatrix}
	\prescript{o}{}{\bm{\theta}_{t,vrt}}\\
	\prescript{o}{}{\bm{r}_{t,vrt}}
\end{bmatrix}
\end{equation}
dove ${\bm{\varphi}_{vrt,g}}$ e $\bm{\varphi}$ sono i vettori di disallineamento tra $\langle vrt \rangle$ e $\langle g \rangle$ e tra $\langle t \rangle$ e $\langle vrt \rangle$, dove $\bm{\varphi}$ è il nuovo valore su cui calcolare il riferimento di velocità in \ref{riferimento_velocita}. Con queste nuove definizioni l'equazione per il calcolo del riferimento di velocità risulta ancora essere uguale a \eqref{riferimento_velocita}. 

Inizialmente a riga \ref{vel_vrt} viene calcolata la velocità con cui si muove la terna virtuale, dopo di che se la terna si sta allontanando a riga \ref{decrease_func} viene richiamata la funzione $\mbox{rallenta}(\cdots)$ che ha il compito di diminuire ${\dot{\bm x}}_{vrt,g}$ se $\varphi_{min}<\norm{\bm{\varphi}}$ (se $ \norm{\bm{\varphi}} > \varphi_{max}$ la terna virtuale $\langle vrt \rangle$ si ferma per aspettare la terna tool $\langle t \rangle$). Per l'implementazione di questa funzione si può utilizzare la funzione di attivazione relativa al obiettivo di controllo $ x_{(j)} \geq x_{(j),m} $ \eqref{funzione_attivazione_maggiore} opportunamente parametrizzata. Infine a riga \ref{aggiornamento_vrt} viene richiamata la funzione $\mbox{aggiorna}(\cdots)$ che ha il compito di aggiornare la nuova posizione della terna virtuale sulla base della nuova velocità ${\dot{\bm x}}_{vrt,g}$.

\begin{algorithm}{TernaVirtuale}[$\prescript{o}{vrt}{\bm T}$,$\bm{\varphi}$,$\bm{\varphi}_{vrt,g}$,$x_{min}$,$x_{max}$,$\gamma$,$dt$]
	\caption{Terna virtuale}
	\label{alg:terna virtuale}
	\begin{algorithmic} [1]
		\STATE{$\dot{\bm{x}}_{vrt,g} = \gamma \bm{\varphi}_{vrt,g}$}
		\label{vel_vrt}
		\IF{$\norm{\bm{\varphi} +{\dot{\bm{x}}}_{vrt,g} dt} > \norm{\bm{\varphi}} $}
%		\ELSE
			\STATE{${\dot{\bm x}}_{vrt,g} \leftarrow {\dot{\bm x}}_{vrt,g} \mbox{rallenta}(\varphi_{min},\varphi_{max},0,1,\norm{{\dot{\bm x}}_{vrt,g}})$}
			\label{decrease_func}
		\ELSE
			\STATE{${\dot{\bm x}}_{vrt,g} \leftarrow {\dot{\bm x}}_{vrt,g}$}			
		\ENDIF
		\STATE{$\prescript{o}{vrt}{\bm T} \leftarrow\mbox{ aggiorna}(\prescript{o}{vrt}{\bm T}, {\dot{\bm x}}_{vrt,g})$}
		\label{aggiornamento_vrt}
		\RETURN{$\prescript{o}{vrt}{\bm T}$}
	\end{algorithmic}
\end{algorithm}

\subsection{Gerarchia dei task}
\label{gerarchia_task}
Facendo riferimento a quanto visto in \ref{Sec_priority_task}, per ogni task è necessario definire Jacobiano, riferimento di velocità e funzione di attivazione.

Per il calcolo dei riferimenti di velocità si utilizza la \eqref{velocita_riferimento_uguaglianza} per gli obiettivi di uguaglianza e la \eqref{velocita_riferimento_disuguaglianza} per gli obiettivi di disuguaglianza. Per la matrice di attivazione del solo task di posizionamento dell'end-effecor, dato che va mantenuto sempre attivo, si è utilizzata una matrice identica. Per tutti gli altri task si utilizza una matrice diagonale i cui elementi sono le \eqref{funzione_attivazione_minore} per gli obiettivi di controllo $\bm{x} \leq \bm{x}_{M}$ e le \eqref{funzione_attivazione_maggiore} per $\bm{x} \geq \bm{x}_{m}$, i cui parametri vanno opportunamente regolati. Per approfondimento sul calcolo degli jacobiani si rimanda a \cite{deliverable5.1}.

Si fa notare come gli jacobiani ed i riferimenti di velocità possano essere  proiettati su una terna qualsiasi purché sia la stessa per entrambi. Nell'implementazione fatta per questa tesi si è scelto di proiettarli sulla terna veicolo. Quanto detto ha significato solamente per i riferimenti di velocità che sono proiettati su qualche terna, non è il caso per esempio del riferimento di velocità relativo al task di limiti di giunto.

La struttura dell'algoritmo è presentata in \algname\ref{alg:priority_task} dove al generico task, $\bm{\rho_{p}}$ rappresenta la soluzione corrente e $ \bm{Q_{p}} $ è il proiettore nel nullo dei task a priorità più alta. I pedici $jl$, $m$, $ha$ e $t$ stanno rispettivamente per limiti di giunto, manipolabilità, assetto orizzontale e posizionamento end-effector.
\begin{algorithm}
	\caption{Gerarchia task}
	\label{alg:priority_task}
	\begin{algorithmic} [1]
		\STATE{$\bm{\rho}_{p} \leftarrow \bm{0}$}
		\STATE{$\bm{Q}_{p} \leftarrow \bm{I}$}
		\STATE{$(\bm{Q}_{p},\bm{\rho}_{p}) \leftarrow \mbox{task}(\bm{A}_{jl},\bm{J}_{jl},\dot{\bar{\bm{x}}}_{jl},\bm{Q}_{p},\bm{\rho}_{p})$}
		\STATE{$(\bm{Q}_{p},\bm{\rho}_{p}) \leftarrow \mbox{task}(A_{m},\bm{J}_{m},\dot{\bar{x}}_{m},\bm{Q}_{p},\bm{\rho}_{p})$}
		\STATE{$(\bm{Q}_{p},\bm{\rho}_{p}) \leftarrow \mbox{task}(\bm{A}_{ha},\prescript{v}{}{\bm{J}_{ha}},\prescript{v}{}{\dot{\bar{\bm{x}}}}_{ha},\bm{Q}_{p},\bm{\rho}_{p})$}
		\STATE{$(\bm{Q}_{p},\bm{\rho}_{p}) \leftarrow \mbox{task}(\bm{A}_{t},\prescript{v}{}{\bm{J}_{t}},\prescript{v}{}{\dot{\bar{\bm{x}}}}_{t},\bm{Q}_{p},\bm{\rho}_{p})$}
		\STATE{$\dot{\bar{\bm{y}}} = \bm{\rho}_{p}$}
	\end{algorithmic}
\end{algorithm}