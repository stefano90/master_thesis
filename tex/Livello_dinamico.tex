
\section{Livello Dinamico}
In questa sezione vengono inizialmente presentati i modelli che è possibile utilizzare per la  descrizione della dinamica del UVMS. Si tiene presente che per il lavoro svolto in questa tesi gli effetti idrodinamici verranno trascurati per entrambi i modelli. Successivamente, indipendentemente dal modello dinamico utilizzato, vengono proposte due leggi di controllo per il calcolo dell'azione ai giunti $\bm{m}$ da utilizzare per controllare il sistema complessivo. 

\subsection{Modello a coordinate generalizzate}
\begin{figure}[ht]
	\centering
	\includegraphics[scale=1, keepaspectratio]{modello_coordinate_generalizzate}
	\caption{Disposizione giunti fittizi del veicolo per modello alle coordinate generalizzate}
	\label{fig:modello_coordinate_generalizzate}
\end{figure}
Questo modello prevede di descrivere il sistema complessivo come un unica catena cinematica cosi da ricondursi alle equazioni \eqref{modello_dinamico}. Nello specifico viene rappresentato il veicolo come supportato da una disposizione meccanica fittizia in accordo con quanto descritto in \figurename~\ref{fig:modello_coordinate_generalizzate}: prima i 3 giunti traslazionali, i quali potendosi estendere fino all'infinito rappresentano la posizione del veicolo nello spazio e poi i 3 giunti rotazionali sovrapposti in configurazione roll. pitch e yaw per descriverne l'orientazione. La massa e l'inerzia del veicolo sono condensate tutte nel sesto ed ultimo dei giunti fittizi (la massa e l'inerzia dei primi cinque è zero).

In questo caso è possibile caratterizzare le variabili da dare in ingresso al Newton Eulero nel modo seguente:
\begin{equation}
\bm{\vartheta} =
	\begin{bmatrix}
		\bm{\eta} \\
		\bm{q}	
	\end{bmatrix}, \quad
	\dot{\bm{\vartheta}} =
	\begin{bmatrix}
		\dot{\bm{\eta}} \\
		\dot{\bm{q}}	
	\end{bmatrix}
\end{equation}
\subsection{Modello di Fossen Esteso}
\label{Sec_modello_di_fossen_esteso}
Un altra possibilità è quella di utilizzare il modello di Fossen esteso, il quale escludendo gli effetti idrodinamici in \eqref{modello_fossen_esteso} risulta essere:
\begin{equation}
\label{modello_fossen_esteso_semplificato}
	\tilde{\bm{M}}(\bm{q})
	\begin{bmatrix}
		\dot{\bm{\nu}} \\
		\ddot{\bm{q}}
	\end{bmatrix} 
	+ \tilde{\bm{C}}(\bm{q},\bm{\nu},\dot{\bm{q}},\dot{\bm{\nu}})
	\begin{bmatrix}
		\bm{\nu} \\
		\dot{\bm{q}}
	\end{bmatrix}
	+ \tilde{\bm{G}}(\bm{\eta}_{2}) = 
	\begin{bmatrix}
		\bm{\tau} \\
		\bm{m}
	\end{bmatrix}, \quad 
	\dot{\bm{\eta}} = \bm{J}^{-1}(\bm{\eta_{2}})\bm{\nu}
\end{equation}
dove $\tilde{\bm{M}}(\bm{q})$ è la matrice di inerzia, $\tilde{\bm{C}}(\bm{q},\bm{\nu},\dot{\bm{q}},\dot{\bm{\nu}})$ è la matrice che tiene conto degli effetti di Coriolis e $\tilde{\bm{G}}(\bm{\eta}_{2})$ la matrice che tiene conto degli effetti gravitazionali. Il vantaggio di utilizzare questo modello consiste nel fatto che tutte le quantità presenti sia in \eqref{modello_fossen_esteso} che in \eqref{modello_fossen_esteso_semplificato} dipendono direttamente da quantità misurabili da appositi sensori a bordo del UVMS. Al contrario si può mostrare come utilizzando il modello a coordinate generalizzate questo non sia più vero, sebbene sia sempre possibile calcolare tali quantità indirettamente esse richiederebbero conti aggiuntivi. 

\begin{figure}[ht]
	\centering
	\includegraphics[scale=1, keepaspectratio]{interpretazione_fossen_ne}
	\caption{Interpretazione modello di Fossen: all'istante $t$ la terna $\langle v' \rangle$ viene fatta coincidere con la terna $\langle v \rangle$ e viene rigidamente connessa con la terna $\langle o \rangle$; $\langle v \rangle$ risulta istantaneamente supportate da sei giunti fittizi, tre rotazionali e tre traslazionali }
	\label{fig:interpretazione_fossen_ne}
\end{figure}

Questo modello è differente da \eqref{modello_dinamico} in quanto le componenti di $\bm{\nu}$, e nello specifico la parte relativa alla velocità angolare, non possono essere associate ad alcuna disposizione meccanica fittizia dei giunti (cosa che invece è stata possibile per $\dot{\bm{\eta}}$). Per questa motivazione non è possibile risolvere questo problema con l'algoritmo di Newton Eulero presentato in \ref{Sec_newton_eulero} in maniera diretta. Viene data la seguente interpretazione al modello \eqref{modello_fossen_esteso_semplificato}:
\begin{enumerate}
	\item al generico istante $t$ una terna ausiliaria $\langle v' \rangle$ viene fatta coincidere con $\langle v \rangle$, ma fissata con la terna inerziale $\langle o \rangle$. In queste condizioni l'UVMS può essere considerato come istantaneamente supportato da due serie di giunti, tre rotazionali e tre traslazionali agenti tra $\langle v \rangle$ e $\langle v' \rangle$ similmente a quanto visto per il modello a coordinate generalizzate (\figurename~\ref{fig:interpretazione_fossen_ne}). La dinamica dell'UVMS risulta ancora rappresentata dall'equazione \eqref{modello_fossen_esteso_semplificato}, anche se valutata come catena robotica standard rispetto a $\langle v' \rangle$;
	
	\item in seguito ad un incremento di tempo $dt$, $\langle v \rangle$ esegue una rotazione infinitesima $\bm{\nu_{2}}dt$ e una traslazione infinitesima $\bm{\nu_{1}}dt$ rispetto a $\langle v' \rangle$. Sotto queste ipotesi la velocità complessiva del veicolo evolve come $\bm{\nu}(t+dt) = \bm{\nu}(t) + \dot{\bm{\nu}}dt$  e la postura del veicolo evolve come $\bm{\eta}(t+dt) = \bm{\eta}(t) + \dot{\bm{\eta}}dt$ con $\dot{\bm{\eta}}$ calcolata dalla relazione Jacobiana in \eqref{modello_fossen_esteso_semplificato}. Al termine della rotazione/tralsazione infinitesima, la terna $\langle v' \rangle$ viene nuovamente fatta coincidere con $\langle v \rangle$ e fissata con la terna inerziale. L'intero processo viene ripetuto così da riottenere la \eqref{modello_fossen_esteso_semplificato} dopo ogni istante di tempo $dt$.
\end{enumerate}

Utilizzando questo modello le variabili da dare in ingresso all'algoritmo di Newton Eulero vengono caratterizzate come segue:
\begin{equation}
\bm{\vartheta} =
	\begin{bmatrix}
		\bm{0} \\
		\bm{q}
	\end{bmatrix}, \quad
\dot{\bm{\vartheta}} =
	\begin{bmatrix}
		\bm{\nu} \\
		\dot{\bm{q}}
	\end{bmatrix}
\end{equation}
dove si osserva come $\bm{\vartheta}$ e $\bm{\dot{\vartheta}}$ siano un riordinamento delle variabile $\bm{c}$ e $\dot{\bm{y}}$ definite rispettivamente in \eqref{def_configurazione} e \eqref{def_velocita}.

Per il lavoro svolto in questa tesi si è utilizzato il modello \eqref{modello_fossen_esteso_semplificato} con la caratterizzazione delle variabili appena data.
\subsection{Generazione coppie di attuazione ai giunti}
\begin{figure}[ht]
	\centering
	\includegraphics[scale=1.2, keepaspectratio]{computed_torque}
	\caption{Schema a blocchi della legge di controllo a computed torque utilizzata. L'azione di coppia ai giunti viene mandata al blocco $SIM$ che rappresenta la simulazione del sistema}
	\label{fig:computed_torque}
\end{figure}
Dopo aver caratterizzato la traiettoria $\bm{\vartheta}$, $\dot{\bm{\vartheta}}$, è necessario inseguirla generando una coppia di attuazione ai giunti $\bm{m}$ congruente. Una possibilità è quella di utilizzare la seguente legge di controllo:
\begin{equation}
	\label{computed_torque}
	\bar{\bm{m}} = \bm{A}(\bm{\vartheta})\bm{K}_{v}\dot{\bm{e}} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bm{\vartheta}} + \bm{C}(\bm{\vartheta})
\end{equation}
con  $\dot{\bm{e}} \triangleq \dot{\bar{\bm{\vartheta}}}-\dot{\bm{\vartheta}}$ errore di velocità e $\bm{K}_{v} \triangleq K_{v}\bm{I}$ è una matrice diagonale che rappresenta il guadagno sull'errore di velocità (\figurename~\ref{fig:computed_torque}). La \eqref{computed_torque} fa parte della più generale famiglia di leggi di controllo \textit{computed torque} che ha la seguente forma:
\begin{equation}
	\bar{\bm{m}} = \bm{A}(\bm{\vartheta})(\ddot{\bar{\bm{\vartheta}}}+\bm{K}_{v}\dot{\bm{e}}+\bm{K}_{p}\bm{e}) + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bm{\vartheta}} + \bm{C}(\bm{\vartheta})
\end{equation}
dove $\ddot{\bar{\bm{\vartheta}}}$ è l'eventuale feedforward di accelerazione ed $\bm{e}$ l'errore di posizione

La $\bar{\bm{m}}$ in \eqref{computed_torque} viene calcolata con la seguente iterazione di Newton-Eulero:
\begin{equation}
	\bar{\bm{m}} = \mbox{NewtonEulero}(\bm{K}_{v}\bm{e},\bm{\dot{\bm{\vartheta}}},\bm{\vartheta},\prescript{v}{}{\bm{g}},\bm{0},\bm{0})
\end{equation}
dove si fa notare l'utilizzo di $\prescript{v}{}{\bm{g}}$ al posto di $\prescript{o}{}{\bm{g}}$ come in \eqref{notazione_newton_eulero} dato che il modello di Fossen descritto  da \eqref{modello_fossen_esteso_semplificato} viene interpretato come una catena robotica a partire da $\langle v' \rangle$.

Un altra possibilità è quella di utilizzare quella che viene chiamato \textit{computed torque referenced}:
\begin{equation}
	\label{computed_torque_referenced}
	\bar{\bm{m}} = \bm{A}(\bm{\vartheta})\bm{K}_{v}\dot{\bm{e}} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bar{\bm{\vartheta}}} + \bm{C}(\bm{\vartheta})
\end{equation}
che differisce dalla \eqref{computed_torque} solo per il termine $\bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bar{\bm{\vartheta}}}$ il quale si può dimostrare che è possibile calcolarlo come segue:
\begin{equation}
\bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bar{\bm{\vartheta}}} = \frac{1}{2}\bm{B}(\bm{\vartheta},\dot{\bar{\bm{\vartheta}}})\dot{\bar{\bm{\vartheta}}} + \frac{1}{2}\bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bm{\vartheta}}
\end{equation}
dove i due termini possono essere calcolati da altrettante iterazioni del Newton-Eulero:
\begin{align*}
	&\bm{B}(\bm{\vartheta},\dot{\bar{\bm{\vartheta}}})\dot{\bar{\bm{\vartheta}}} = \mbox{NewtonEulero} (\bm{0},\dot{\bar{\bm{\vartheta}}},\bm{\vartheta},\bm{0},\bm{0},\bm{0}),\\
	&\bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bm{\vartheta}} = \mbox{NewtonEulero} (\bm{0},\dot{\bm{\vartheta}},\bm{\vartheta},\bm{0},\bm{0},\bm{0})
\end{align*}
Lo schema a blocchi complessivo è analogo a quello di \figurename~\ref{fig:computed_torque}.

Nel caso di utilizzo in un sistema fisico la $\bar{\bm{m}}$ calcolata in \eqref{computed_torque} o in \eqref{computed_torque_referenced} viene data direttamente o indirettamente al manipolatore a secondo che quest'ultimo possa essere controllato in coppia e/o in velocità. La $\ddot{\bm{\vartheta}}$ viene quindi misurata per mezzo di appositi sensori. 
