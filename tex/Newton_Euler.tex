
\section{Algoritmo di Newton-Eulero}
\label{Sec_newton_eulero}
Viene qui di seguito presentato l'algoritmo di Newton-Eulero il quale svolge un ruolo molto importante sia per la parte di controllo che per la parte di simulazione della dinamica dei manipolatori.
\subsection{Introduzione}
Il modello dinamico di una catena cinematica è descritto dal seguente sistema di equazioni differenziali:
\begin{equation}
\label{modello_dinamico}
\bm{A}(\bm{\vartheta})\ddot{\bm{\vartheta}} + \bm{B}(\bm{\vartheta},\dot{\bm{\vartheta}})\dot{\bm{\vartheta}} + \bm{C}(\bm{\vartheta}) = \bm{m} + \bm{J}^{T}\bm{f}_{ext}, \quad \bm{f}_{ext} \triangleq \begin{bmatrix} \bm{N}_{ext} \\ \bm{F}_{ext}\end{bmatrix}
\end{equation}
dove $ \bm A(\bm{\vartheta})$ è la matrice di inerzia, $\bm B(\bm{\vartheta},\dot{\bm{\vartheta}})$ è la matrice che tiene conto degli effetti di Coriolis, $\bm C(\bm{\vartheta})$ tiene conto dei termini gravitazionali, $\bm{\vartheta}$, $\dot{\bm{\vartheta}}$, $\ddot{\bm{\vartheta}}$ i vettori posizione, velocità ed accelerazione ai giunti, $\bm{m}$ il vettore di forza/coppia ai giunti (a seconda che siano giunti traslazionali o rotazionali) e $\bm{J}^{T}\bm{f}_{ext}$ è la proiezione nello spazio dei giunti del vettore forza/coppia eventualmente presente sull'organo terminale (esercitata dall'end-effector verso l'ambiente). 

L'algoritmo di Newton-Eulero è un possibile approccio per risolvere il cosiddetto \textit{problema dinamico inverso}, ossia  dedurre $ \bm m $ sulla base della conoscenza delle $ \bm{\vartheta},\dot{\bm{\vartheta}},\ddot{\bm{\vartheta}} $ ed eventualmente $\bm{f}_{ext}$. Quest'ultimo attraverso una procedura ricorsiva propaga le velocità e le accelerazioni (lineari ed angolari) di ogni link del manipolatore, dalla base verso l'end-effector (\textit{fase forward}), per poi dedurre forze e momenti congruenti  considerando eventualmente delle forze esterne agenti sull'organo terminale (\textit{fase backward}).

Si osserva come, affinchè $ \bm{\vartheta} $, $ \dot{\bm{\vartheta}} $, $ \ddot{\bm{\vartheta}} $ siano considerate come variabili di posizione, velocità e accelerazione ai giunti, è necessario che $ \bm{\vartheta} $ sia considerato come vettore di configurazione di una catena cinematica. Per questo nel caso in cui si considera un manipolatore a base fissa $ \bm{\vartheta} \equiv \bm{q} $, mentre nel caso in cui si considera un manipolatore a base mobile, è necessario che le variabili che descrivano il veicolo (tipicamente la posizione in coordinate cartesiane e l'orientazione per mezzo degli angoli di Eulero) siano associate ad una catena cinematica fittizia, ossia $\bm{\vartheta} \equiv \begin{bmatrix} \bm{\eta}^{T} &\bm{q}^T \end{bmatrix}^{T}$. Fatti questi opportuni accorgimenti, non c'è problema a far valere il modello dinamico di \eqref{modello_dinamico} anche per un manipolatore a base mobile.
\subsection{Fase forward}
Questa fase anche chiamata calcolo delle grandezze cinematiche, ha come unico scopo quello di calcolare le quantità necessarie per la fase successiva che come vedremo sono le velocità/accelerazioni angolari e lineari di ogni link. Vengono qui riportate le formule ricorsive per il calcolo delle velocità: 
\begin{align}
	\label{velocita_ang_newton_eulero}
	\bm \omega_{i} &=
	\begin{cases} 
		\bm \omega_{i-1}+\bm k_{i}\dot{\bm{\vartheta}}_{i}, & \mbox{per }i\mbox{ rot.} \\ 
		\bm \omega_{i-1}, & \mbox{per }i\mbox{ tras.}
	\end{cases}
	\\
	\label{velocita_lin_newton_eulero}
	\bm v_{i} &=
	\begin{cases} 
		\bm v_{i-1}+\bm \omega_{i-1}\wedge \bm r_{i,i-1}, & \mbox{per }i\mbox{ rot.} \\
		\bm v_{i-1}+\bm \omega_{i-1}\wedge \bm r_{i,i-1}+\bm k_{i}\dot{\bm{\vartheta}}_{i}, & \mbox{per }i\mbox{ tras.}
	\end{cases}
\end{align}
dove $ \bm r_{i,i-1} $ è il vettore che collega il giunto $ i-1 $ al giunto $ i $ e $ \bm k_{i} $ è il versore di rotazione/traslazione del giunto $ i $. Perciò una volta inizializzate le $ \bm \omega_{0} $ e $ \bm v_{0} $ è possibile calcolare le $ \bm \omega_{i} $, $ \bm v_{i} $ percorrendo la catena cinematica dalla base fino all'end-effector ($ i = 1,..,N $). Derivando rispetto al tempo la \eqref{velocita_ang_newton_eulero} e la \eqref{velocita_lin_newton_eulero} si ottengono:
\begin{align}
	\label{accelerazioni_ang_newton_eulero}
	\dot{\bm \omega}_{i} &= 
	\begin{cases} 
		\dot{\bm \omega}_{i-1} + (\bm \omega_{i-1}\wedge \bm k_{i})\dot{\bm{\vartheta}}_{i} + \bm k_{i} \ddot{\bm{\vartheta}}_{i}, & \mbox{per }i\mbox{ rot.} \\ 
		\dot{\bm \omega}_{i-1}, & \mbox{per }i\mbox{ tras.}
	\end{cases}
	\\
	\label{accelerazioni_lin_newton_eulero}
	\dot{\bm v}_{i} &= 
	\begin{cases}
		\dot{\bm v}_{i-1}+\dot{\bm \omega}_{i-1}\wedge \bm r_{i,i-1} + \bm \omega_{i-1} \wedge (\bm \omega_{i-1} \wedge \bm r_{i,i-1}), & \mbox{per }i\mbox{ rot.} \\ 
		\dot{\bm v}_{i-1}+\dot{\bm \omega}_{i-1}\wedge \bm r_{i,i-1} + \bm \omega_{i-1} \wedge (\bm \omega_{i-1} \wedge \bm r_{i,i-1} + \bm k_{i}\dot{\bm{\vartheta}}_{i}) \\ 
		\qquad \qquad \qquad \qquad \qquad \qquad + (\bm \omega_{i-1} \wedge \bm k_{i})\dot{\bm{\vartheta}}_{i} + \bm k_{i}\ddot{\bm{\vartheta}}_{i}, & \mbox{per }i\mbox{ tras.}
	\end{cases}
\end{align}
che sono le formule per il calcolo delle accelerazioni angolari e lineari $ \dot{\bm \omega}_{i} $, $ \dot{\bm v}_{i} $. Ovviamente anche in \eqref{accelerazioni_ang_newton_eulero}, \eqref{accelerazioni_lin_newton_eulero} è necessaria l'inizializzazione delle $ \dot{\bm \omega}_{0} $, $ \dot{\bm v}_{0} $. 

Nello specifico $ \bm \omega_{i} $, $ \bm v_{i} $, $ \dot{\bm \omega}_{i} $ e $ \dot{\bm v}_{i} $, rappresentano le velocità e accelerazioni angolari e lineari dell'estremo del link $ i $. Per motivi che si vedranno nella prossima sezione, quelle che interessano sono le velocità e accelerazioni baricentrali. Mentre le velocità angolari baricentrali (e così anche le accelerazioni angolari) corrispondono a quelle precedentemente calcolate, le velocità e accelerazioni lineari devono essere calcolate utilizzando la seguente formula:
\begin{equation}
\label{velocita_lin_baricentrali}
\bm v{_{C}}_i = \bm v_{i} + \bm \omega_{i} \wedge \bm r_{C_{i},i}
\end{equation}
la quale derivata porta a:
\begin{equation}
\label{accelerazioni_lin_baricentrali}
\dot{\bm v}{_{C}}_i = \dot{\bm v}_{i} + \dot{\bm \omega}_{i} \wedge \bm r_{C_{i},i} + \bm \omega_{i} \wedge (\bm \omega_{i} \wedge \bm r_{C_{i},i})
\end{equation}

Essendo le \eqref{velocita_ang_newton_eulero}, \eqref{velocita_lin_newton_eulero}, \eqref{accelerazioni_ang_newton_eulero}, \eqref{accelerazioni_lin_newton_eulero} in forma geometrica, per poterle implementare su calcolatore occorre proiettarle sulla terna $ \langle i \rangle $ e pre-moltiplicare tutte le quantità espresse in terna $ \langle i-1 \rangle $ per la matrice di rotazione $ \prescript{i-1}{i}{\mathbf{\bm R}}^{T} $.
\subsection{Fase backward}
\begin{figure}[ht]
	\centering
	\includegraphics[scale=1.2, keepaspectratio]{newton_eulero}
	\caption{Forze e momenti agenti sull'i-esimo link}
	\label{fig:newton_eulero}
\end{figure}
Per la deduzione delle formule ricorsive di questa fase, si fa riferimento a \figurename~\ref{fig:newton_eulero} che descrive forze e momenti agenti sul generico link $i$. $Q_{i}^{+}$ e $Q_{i}^{-}$ rappresentano gli estremi del link, $C_{i}$ il centro di massa e $\bm{F}_{i-1,i}$, $\bm{N}_{i-1,i} $, $-\bm{F}_{i,i+1}$, $-\bm{N}_{i,i+1}$ forza e momenti esercitati dai link $i-1$ e $i+1$ sul link $i$. Una volta inizializzate opportunamente $\bm{F}_{N,N+1}$, $\bm {N}_{N,N+1}$ che rappresentano, se presenti, le eventuali forze e momenti agenti dall'organo terminale verso l'esterno (nel seguito verranno indicate rispettivamente con $\bm{F}_{ext}$ e $\bm{N}_{ext}$), le quantità in \figurename~\ref{fig:newton_eulero} sono tutte note tranne $\bm{F}_{i-1,i}$, $ \bm{N}_{i-1,i} $ le quali possono essere calcolate utilizzando le equazioni di Eulero riportate qui per completezza:
\begin{equation}
\label{equazioni_eulero}
\begin{cases} 
\bm R_{i} = M_{i} \dot{\bm v}{_{C}}_{i} \\ 
\bm \tau_{i} = \bm I \dot{\bm \omega}_{i} + \bm \omega_{i} \wedge \bm I_{i} \bm \omega_{i}
\end{cases}
\end{equation}
con $\bm{R}_{i}$ e $\bm{\tau}_{i}$ forze e momenti risultanti, $M_{i}$ massa del link, $\dot{\bm{v}}{_{C}}_{i}$ velocità lineare del baricentro, $\bm{I}_{i} $ il tensore di inerzia baricentrale e $ \bm{\omega}_{i} $, $\dot{\bm{\omega}}_{i} $ rispettivamente velocità e accelerazione angolare del link. 

A questo punto utilizzando la \eqref{equazioni_eulero} e facendo sempre riferimento a \figurename~\ref{fig:newton_eulero}, le formule risultano essere:
\begin{equation}
\label{forze_momenti_congruenti}
\begin{cases} 
\bm F_{i-1,i} = \bm F_{i,i+1} - M_{i} \bm g + M_{i} \dot{\bm v}_{C_{i}} \\ 
\bm N_{i-1,i} = \bm N_{i,i+1} - (\bm r_{{Q}_{i}^{-},C_{i}} \wedge \bm F_{i-1,i}) + (\bm r_{{Q}_{i}^{+},C_{i}} \wedge \bm F_{i,i+1}) + \bm I_{i}\dot{\bm \omega}_{i} + \bm \omega_{i} \wedge \bm I \bm \omega_{i}
\end{cases}
\end{equation}
dove si notano in particolare i termini  $  (\bm r_{{Q}_{i}^{-},C_{i}} \wedge \bm F_{i-1,i}) $ e $  (\bm r_{{Q}_{i}^{+},C_{i}} \wedge \bm F_{i,i+1}) $ che sono i momenti indotti rispettivamente da $ \bm F_{i-1,i} $ e $ \bm F_{i,i+1} $ non applicate sul baricentro. 

La \eqref{forze_momenti_congruenti} essendo in forma geometrica, così come le \eqref{velocita_ang_newton_eulero} \eqref{velocita_lin_newton_eulero} \eqref{accelerazioni_ang_newton_eulero} \eqref{accelerazioni_lin_newton_eulero}, deve anche'essa essere proiettata sul frame $ \langle i \rangle $ affinché possa essere direttamente implementata, questa volta però premoltiplicando per $ \prescript{i}{i+1}{\mathbf{\bm R}} $ le quantità espresse in terna $ \langle i+1 \rangle $ dato che si sta percorrendo la catena cinematica in senso opposto.

Per indicare una iterazione dell'algoritmo di Newton-Eulero nel seguito si utilizzerà la seguente notazione:
\begin{equation}
\label{notazione_newton_eulero}
	\bm{m} = \mbox{NewtonEulero}(\ddot{\bm{\vartheta}},\dot{\bm{\vartheta}},\bm{\vartheta},\prescript{o}{}{\bm{g}},\prescript{t}{}{\bm{F}_{ext}},\prescript{t}{}{\bm{N}_{ext}})
\end{equation}
dove $\bm{m}$, $\ddot{\bm{\vartheta}}$, $\dot{\bm{\vartheta}}$ e $\bm{\vartheta}$ hanno lo stesso significato di \eqref{modello_dinamico}, $\prescript{o}{}{\bm{g}}$ è la rappresentazione algebrica del vettore gravità con componenti in terna $\langle o \rangle$, $\prescript{t}{}{\bm{F}_{ext}}$ e $\prescript{t}{}{\bm{N}_{ext}}$ sono forza e momenti esterni agenti dall'end-effector sull'ambiente con componenti in $\langle t \rangle$.